#!/bin/sh
#
# This is the build script for the Sauna Twitter frontend component.
#
# Run it from the project root,
# i.e. from the folder which contains the 'frontend' subfolder.
#

cd frontend

npm install
npm run build
