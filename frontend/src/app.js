import React from "react";
import ReactDOM from "react-dom";
import './app.css';

const cellSize = "50px";

const colors = {
  0: 'gray',
  1: 'red',
  2: 'yellow'
};

class Board extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      matrix: [
        [2, 0, 0],
        [0, 1, 0],
        [2, 0, 1]]
    };

  }


  render() {

    const rows = this.state.matrix;

    if (!rows || rows.length === 0) {
      return <div>Empty board...</div>
    }

    let key = 0;


    return <div className="board" style={
      {
        gridTemplateColumns: `repeat(${rows[0].length}, ${cellSize})`,
        gridAutoRows: `${cellSize}`
      }
    }>
      {rows.map(row => row.map(cell => <Cell key={key++} colorIndex={cell}/>))}
    </div>;
  }
}

function Cell(props) {
  return <div
    style={{ backgroundColor: colors[props.colorIndex] }}
  />;
}

const mountNode = document.getElementById("app");
ReactDOM.render(<Board/>, mountNode);